#!/usr/bin/env python3

from aws_cdk import core

from iac.iac_stack import IacStack


app = core.App()
myEnv = core.Environment(region="eu-west-1", account="126895207637")
IacStack(app, "apprentice-5", env=myEnv)

app.synth()
