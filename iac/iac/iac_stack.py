from aws_cdk import (
    core,
    aws_ec2 as ec2,
    aws_ecs as ecs,
    aws_iam as iam,
    aws_ecr as ecr,
    aws_elasticloadbalancingv2 as elbv2,
    aws_logs as logs,
)
import os


class IacStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # VPC and Cluster Configuration
        vpc = ec2.Vpc.from_lookup(self, id="VPC", vpc_id="vpc-02b318197ae37afd5")

        cluster = ecs.Cluster(self, "apprentice-5", vpc=vpc)
        cluster.add_default_cloud_map_namespace(name="apprentice-5")

        service_principal = iam.ServicePrincipal('ecs-tasks.amazonaws.com')
        task_role = iam.Role(self, 'ecsTaskExecutionRole-apprentice-5', assumed_by=service_principal)
        ecs_managed_policy = iam.ManagedPolicy.from_aws_managed_policy_name(
            'service-role/AmazonECSTaskExecutionRolePolicy'
        )
        task_role.add_managed_policy(ecs_managed_policy)

        frontend_ecr_repo = ecr.Repository.from_repository_name(
            self,
            id="frontend-apprentice-5",
            repository_name="apprentice-5-frontend"
        )
        backend_ecr_repo = ecr.Repository.from_repository_name(
            self,
            id="backend-apprentice-5",
            repository_name="apprentice-5-backend",
        )

        frontend_security_group = ec2.SecurityGroup(
            self,
            'frontend-sg-apprentice-5',
            allow_all_outbound=True,
            security_group_name="FrontendSG-apprentice-5",
            vpc=vpc
        )
        backend_security_group = ec2.SecurityGroup(
            self,
            'backend-sg-apprentice-5',
            allow_all_outbound=True,
            security_group_name="BackendSG-apprentice-5",
            vpc=vpc
        )
        
        database_security_group = ec2.SecurityGroup(
            self,
            'database-sg-apprentice-5',
            allow_all_outbound=True,
            security_group_name="DatabaseSG-apprentice-5",
            vpc=vpc
        )
        
        frontend_security_group.connections.allow_from_any_ipv4(
            ec2.Port.tcp(5000),
        )
        backend_security_group.connections.allow_from_any_ipv4(
            ec2.Port.tcp(8080),
        )
        database_security_group.connections.allow_from_any_ipv4(
            ec2.Port.tcp(6379)
        )

        # Frontend TaskDefinition
        frontend_task_definition = ecs.FargateTaskDefinition(
            self,
            'frontendTaskDefinition-apprentice-5',
            cpu=256,
            memory_limit_mib=512,
            task_role=task_role,
        )
        
        # Backend TaskDefinition
        backend_task_definition = ecs.FargateTaskDefinition(
            self,
            'backendTaskDefinition-apprentice-5',
            cpu=256,
            memory_limit_mib=512,
            task_role=task_role
        )
        
        # Database TaskDefinition
        database_task_definition = ecs.FargateTaskDefinition(
            self,
            'databaseTaskDefinition-apprentice-5',
            cpu=256,
            memory_limit_mib=1024,
            task_role=task_role
        )

        frontend_log_group = logs.LogGroup(
            self,
            "frontendLogGroup-apprentice-5",
            log_group_name="/ecs/frontendLogGroup-apprentice-5",
            removal_policy=core.RemovalPolicy.DESTROY
        )

        backend_log_group = logs.LogGroup(
            self,
            "backendLogGroup-apprentice-5",
            log_group_name="/ecs/backendLogGroup-apprentice-5",
            removal_policy=core.RemovalPolicy.DESTROY
        )
        
        database_log_group = logs.LogGroup(
            self,
            "databaseLogGroup-apprentice-5",
            log_group_name="/ecs/databaseLogGroup-apprentice-5",
            removal_policy=core.RemovalPolicy.DESTROY
        )
        
        frontend_log_driver = ecs.AwsLogDriver(
            log_group=frontend_log_group,
            stream_prefix="frontend"
        )
        
        backend_log_driver = ecs.AwsLogDriver(
            log_group=backend_log_group,
            stream_prefix="backend",
        )
        
        database_log_driver = ecs.AwsLogDriver(
            log_group=database_log_group,
            stream_prefix="database",
        )

        # Frontend container
        frontend_container = frontend_task_definition.add_container(
            'frontend-apprentice-5',
            image=ecs.ContainerImage.from_ecr_repository(frontend_ecr_repo, tag=os.environ["CI_COMMIT_SHORT_SHA"]),
            logging=frontend_log_driver,
            environment={
                "COLORS_BACKEND_ADDRESS": "backend.apprentice-5:8080",
            },
            memory_limit_mib=512,
        )
        frontend_container.add_port_mappings(ecs.PortMapping(container_port=5000))

        # Backend container
        backend_container = backend_task_definition.add_container(
            'backend-apprentice-5',
            image=ecs.ContainerImage.from_ecr_repository(backend_ecr_repo, tag=os.environ["CI_COMMIT_SHORT_SHA"]),
            logging=backend_log_driver,
            environment={
                "COLORS_DATABASE_ADDRESS": "database.apprentice-5:6379",
            },
            memory_limit_mib=512,
        )
        backend_container.add_port_mappings(ecs.PortMapping(container_port=8080))
        
        # Database container
        database_container = database_task_definition.add_container(
            'database-apprentice-5',
            image=ecs.ContainerImage.from_registry("redis:6.0-alpine"),
            logging=database_log_driver,
        )
        database_container.add_port_mappings(ecs.PortMapping(container_port=6379))

        frontend_service = ecs.FargateService(
            self,
            "Frontend-apprentice-5",
            cluster=cluster,            # Required
            task_definition=frontend_task_definition,
            security_group=frontend_security_group,
            desired_count=1,
            assign_public_ip=True,
            cloud_map_options=ecs.CloudMapOptions(name="frontend"),
            service_name="frontend"
        )

        ecs.FargateService(
            self,
            "Backend-apprentice-5",
            cluster=cluster,            # Required
            task_definition=backend_task_definition,
            security_group=backend_security_group,
            assign_public_ip=True,
            desired_count=1,
            cloud_map_options=ecs.CloudMapOptions(name="backend"),
            service_name="backend"
        )
        
        ecs.FargateService(
            self,
            "Database-apprentice-5",
            cluster=cluster,            # Required
            task_definition=database_task_definition,
            security_group=database_security_group,
            desired_count=1,
            cloud_map_options=ecs.CloudMapOptions(name="database"),
            service_name="database"
        )

        alb = elbv2.ApplicationLoadBalancer(
            self,
            "external-apprentice-5",
            vpc=vpc,
            internet_facing=True
        )

        alb_listener = alb.add_listener(
            'alb-listener-apprentice-5',
            port=80
        )
        
        alb_health_check = elbv2.HealthCheck(path='/')

        alb_listener.add_targets(
            'tg-apprentice-5',
            port=80,
            health_check=alb_health_check,
            targets=[frontend_service]
        )
        
        core.CfnOutput(self, 'ALB DNS: ', value=alb.load_balancer_dns_name)